package com.p0mami.services.providers;

import com.p0mami.models.processes.StochasticProcess;
import com.p0mami.models.realizations.StochasticProcessGeneralRealization;
import com.p0mami.models.realizations.StochasticProcessStreamFunctionalRealization;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class StochasticProcessRealizationProvider {

    private final StochasticProcess stochasticProcess;

    public StochasticProcessRealizationProvider(StochasticProcess stochasticProcess) {
        this.stochasticProcess = stochasticProcess;
    }

    public StochasticProcessGeneralRealization provide(List<Double> time) {
        return new StochasticProcessGeneralRealization(
                time.stream().map(stochasticProcess::getPoint).collect(Collectors.toList())
        );
    }

    public StochasticProcessStreamFunctionalRealization provide(Supplier<DoubleStream> dateGenerator) {
        return new StochasticProcessStreamFunctionalRealization(
                dateGenerator,
                stochasticProcess::getX
        );
    }

    public StochasticProcessStreamFunctionalRealization provide(Supplier<DoubleStream> dateGenerator, int size) {
        return new StochasticProcessStreamFunctionalRealization(
                dateGenerator,
                stochasticProcess::getX,
                size
        );
    }

}

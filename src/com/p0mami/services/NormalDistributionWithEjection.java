package com.p0mami.services;

import java.util.Random;

public class NormalDistributionWithEjection {


    private final double mean;
    private final double sigma;
    private final int interval;
    private final double ejectionSigma;
    private static final Random random = new Random();
    private long step = 0;
    private long ejectionStep;

    public NormalDistributionWithEjection(double mean, double sigma) {
        this.mean = mean;
        this.sigma = sigma;
        this.interval = 200;
        this.ejectionSigma = 200;
        this.ejectionStep = random.nextInt() % interval;
    }

    public NormalDistributionWithEjection(double mean, double sigma, int interval, double ejectionSigma) {
        this.mean = mean;
        this.sigma = sigma;
        this.interval = interval;
        this.ejectionSigma = ejectionSigma;
    }

    public double nextGaussian() {
        double ejection = 0;
        if (step == ejectionStep) {
            ejection = random.nextGaussian() * ejectionSigma;
        }
        step++;
        return random.nextGaussian() * sigma + mean + ejection;
    }

}

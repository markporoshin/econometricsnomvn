package com.p0mami.services.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;

import org.apache.commons.math3.stat.inference.MannWhitneyUTest;
import org.apache.commons.math3.util.Pair;

public class MannWhitneyTest {

    private boolean logging = true;

    public void setLogging(boolean logging) {
        this.logging = logging;
    }

    public Pair<Boolean, Double> test(List<Double> s1, List<Double> s2, double a) {

        MannWhitneyUTest test = new MannWhitneyUTest();
        double pvalue = test.mannWhitneyU(toArray(s1), toArray(s2));
        boolean result = pvalue > a;
        if (logging) {
            System.out.printf("MannWhitneyTest { pvalue: %f, result: %b}\n", pvalue, result);
        }
        return new Pair<>(result, pvalue);
    }

    public Pair<Boolean, Double> test(double[] s1, double[] s2, double a) {

        MannWhitneyUTest test = new MannWhitneyUTest();
        double pvalue = test.mannWhitneyUTest(s1, s2);
        boolean result = pvalue > a;
        if (logging) {
            System.out.printf("MannWhitneyTest { pvalue: %f, result: %b}\n", pvalue, result);
        }
        return new Pair<>(result, pvalue);
    }

    private static double[] toArray(List<Double> s) {
        double[] a = new double[s.size()];
        for (int i = 0; i < s.size(); i++) {
            a[i] = s.get(i);
        }
        return a;
    }

}

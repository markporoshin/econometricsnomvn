package com.p0mami.services.tests;

import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessStreamRealization;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.special.Erf;

import java.util.concurrent.atomic.AtomicReference;

public class PearsonTest {

    public static double xsi_005_8 = 2.7326;

    public boolean test(
            StochasticProcessStreamRealization realization,
            StochasticProcessPolyApproximation approximation,
            int intervals,
            double a
    ) {
        int[] varSeries = new int[intervals];

        long size = realization.getSize();

        AtomicReference<Double> minRef = new AtomicReference<>(0.);
        AtomicReference<Double> maxRef = new AtomicReference<>(0.);
        realization.getPointsStream()
                .mapToDouble((p)->(p.getSecond() - approximation.getX(p.getFirst())))
                .peek((x) -> {
            if (x < minRef.get()) {
                minRef.set(x);
            }
            if (x > maxRef.get()) {
                maxRef.set(x);
            }
        }).filter((t) -> false).findFirst();
        double min = minRef.get() * 1.01;
        double max = maxRef.get() * 1.01;
        double intervalLen = (max - min) / intervals;
        realization.getPointsStream()
                .mapToDouble((p)->(p.getSecond() - approximation.getX(p.getFirst())))
                .peek((x) -> {
            int i = (int)((x - min) / intervalLen);
            varSeries[i]++;
        }).filter((t) -> false).findFirst();
        double probability = normalF(min + intervalLen, 0, approximation.getSigma());
        double sum = ((double) varSeries[0] / size - probability) * ((double) varSeries[0] / size - probability) / probability;
        for (int i = 1; i < intervals - 1; i++) {
            probability = normalF(min + (i + 1) * intervalLen, 0, approximation.getSigma()) - normalF(min + i * intervalLen, 0, approximation.getSigma());
            sum += ((double) varSeries[i] / size - probability) * ((double) varSeries[i] / size - probability) / probability;
        }
        probability = normalF(-(min + (intervals - 1) * intervalLen), 0, approximation.getSigma());
        sum += ((double) varSeries[intervals-1] / size - probability) * ((double) varSeries[intervals-1] / size - probability) / probability;
        double xsi = sum * size;
        return xsi < new ChiSquaredDistribution(intervals - 2).inverseCumulativeProbability(a);
    }

    public static double normalF(double x, double m, double s) {
        return 1. / 2 * (1. + Erf.erf((x - m) / Math.sqrt(2) / s));
    }
}

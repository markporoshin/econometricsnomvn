package com.p0mami.services.tests;

import org.apache.commons.math3.stat.inference.KolmogorovSmirnovTest;
import org.apache.commons.math3.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class SmirnovTest {

    private boolean logging;

    public SmirnovTest() {
        this.logging = true;
    }

    public void setLogging(boolean logging) {
        this.logging = logging;
    }

    boolean test(ArrayList<Double> sample1, ArrayList<Double> sample2, double a) {
        KolmogorovSmirnovTest test = new KolmogorovSmirnovTest();
        double pvalue = test.kolmogorovSmirnovTest(toArray(sample1), toArray(sample2));
        double ksSum =  test.ksSum(a, 1e-5, 10_000);
        if (logging) {
            System.out.printf("SmirnovTest { pvalue: %f, ksSum: %f, result: %b}\n", pvalue, ksSum, pvalue < ksSum);
        }
        return pvalue < ksSum;
    }


    public Pair<Boolean, Double> test(double[] sample1, double[] sample2, double a) {
        KolmogorovSmirnovTest test = new KolmogorovSmirnovTest();
        double pvalue = test.kolmogorovSmirnovTest(sample1, sample2);
        boolean result = pvalue > a;
        if (logging) {
            System.out.printf("SmirnovTest { pvalue: %f, result: %b}\n", pvalue, result);
        }
        return new Pair<>(result, pvalue);
    }


    private static double[] toArray(List<Double> s) {
        double[] a = new double[s.size()];
        for (int i = 0; i < s.size(); i++) {
            a[i] = s.get(i);
        }
        return a;
    }

}

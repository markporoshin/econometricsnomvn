package com.p0mami.services.readers;


import com.p0mami.models.realizations.StochasticProcessGeneralRealization;
import org.apache.commons.math3.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CSVStochasticProcessRealizationReader {
    public static final String COMMA_DELIMITER = ",";

    public StochasticProcessGeneralRealization readTrajectory(String filename) {
        StochasticProcessGeneralRealization stochasticProcess = new StochasticProcessGeneralRealization();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);

                Double t = Double.valueOf(values[0]);
                Double v = Double.valueOf(values[1]);

                stochasticProcess.addPoint(new Pair<>(t, v));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stochasticProcess;
    }
}

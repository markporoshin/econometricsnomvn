package com.p0mami;

import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessGeneralRealization;
import com.p0mami.models.realizations.StochasticProcessStreamFileRealization;
import com.p0mami.models.realizations.StochasticProcessStreamRealization;
import com.p0mami.services.approximation.PolyApproximationService;
import com.p0mami.services.providers.StochasticProcessRealizationProvider;
import com.p0mami.services.tests.MannWhitneyTest;
import com.p0mami.services.tests.PearsonTest;
import com.p0mami.services.tests.SmirnovTest;
import org.apache.commons.math3.util.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ResearchFromFile {

    private static final String SEPARATOR = ", ";
    private static final int MAX_MODEL_DEG = 3;

    public static void main(String[] args) {
        String inputFile = args[0];
        int chunk = Integer.decode(args[1]);
        String dir = args[2];

        BufferedWriter researchWriter = null, sampleWriter = null;
        try {
            researchWriter = new BufferedWriter(new FileWriter(dir+"researchOutput.txt"));
            sampleWriter = new BufferedWriter(new FileWriter(dir+"sampleOutput.csv"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert researchWriter != null;
        assert sampleWriter != null;
        PolyApproximationService approximationService = new PolyApproximationService();
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            long N = Integer.decode(reader.readLine());

            long chunks = N / chunk;
            double prev_percent = 0;
            for (int i = 0; i < chunks; i++) {

                List<Pair<Double, Double>> realizationPoints = new ArrayList<>();
                double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
                for (int j = 0; j < chunk; j++) {
                    String[] line_elements = reader.readLine().split(SEPARATOR);
                    double t = Double.parseDouble(line_elements[0]);
                    double x = Double.parseDouble(line_elements[1]);
                    realizationPoints.add(new Pair<>(t, x));
                    min = Math.min(x, min);
                    max = Math.max(x, max);
                }
                researchWriter.write(
                        String.format("{\n\tdiff=%f; min=%f; max=%f", max - min, min, max)
                );
                StochasticProcessGeneralRealization realization = new StochasticProcessGeneralRealization();
                realization.setPoints(realizationPoints);
                double[][] diffsEachDeg = new double[MAX_MODEL_DEG+1][];
                for (int k = 0; k <= MAX_MODEL_DEG ; k++) {
                    StochasticProcessPolyApproximation approximation = approximationService.approximate(realization, k);
                    double[] diffs = realization.getPoints().stream()
                            .mapToDouble((p)-> p.getSecond() - approximation.getX(p.getFirst())).toArray();
                    int diffSize = diffs.length;
                    double[] firstPart = Arrays.copyOfRange(diffs, 0, diffSize / 2);
                    double[] secondPart = Arrays.copyOfRange(diffs, diffSize / 2, diffSize);
                    MannWhitneyTest mannWhitneyTest = new MannWhitneyTest();
                    SmirnovTest smirnovTest = new SmirnovTest();
                    mannWhitneyTest.setLogging(false);
                    smirnovTest.setLogging(false);
                    Pair<Boolean, Double> mannWhithneyResult = mannWhitneyTest.test(firstPart, secondPart, 0.05);
                    Pair<Boolean, Double> smirnovResult = smirnovTest.test(firstPart, secondPart, 0.05);

                    researchWriter.write(
                            String.format("\n\tk=%d; mw_test={%b, %f}; sm_test={%b, %f}; koef=%s; sigma=%f",
                                    k,
                                    mannWhithneyResult.getFirst(), mannWhithneyResult.getSecond(),
                                    smirnovResult.getFirst(), smirnovResult.getSecond(),
                                    approximation.getBettaStr(),
                                    approximation.getSigma()
                            )
                    );
                    diffsEachDeg[k] = diffs;
                }
                researchWriter.write("\n}\n");
                for (int j = 0; j < chunk; j++) {
                    StringBuilder builder = new StringBuilder();
                    for (int k = 0; k <= MAX_MODEL_DEG; k++) {
                        builder.append(diffsEachDeg[k][j]);
                        builder.append(SEPARATOR);
                    }
                    sampleWriter.write(builder.toString());
                }
                double percent = 100. * i / chunks;
                if (percent - prev_percent > 1) {
                    System.out.printf("\r%.2f", percent);
                    prev_percent = percent;
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            researchWriter.close();
            sampleWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

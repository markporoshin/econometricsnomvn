package com.p0mami.models.realizations;

import com.p0mami.services.readers.CSVStochasticProcessStreamRealizationReader;
import org.apache.commons.math3.util.Pair;

import java.util.stream.Stream;

public class StochasticProcessStreamFileRealization extends StochasticProcessStreamRealization {

    private long size;

    private final String filename;
    private final CSVStochasticProcessStreamRealizationReader realizationReader;
    private Stream<Pair<Double, Double>> currentStream;


    public StochasticProcessStreamFileRealization(String filename) {
        this.size = 0;
        this.filename = filename;
        this.realizationReader = new CSVStochasticProcessStreamRealizationReader();
    }

    @Override
    public Stream<Pair<Double, Double>> getPointsStream() {
        currentStream = realizationReader.readRealization(filename);
        return currentStream;
    }

    @Override
    public void setSize(long size) {
        this.size = size;
    }

    @Override
    public long getSize() {
        return 1;
    }

    public void close() {
        currentStream.close();
    }
}

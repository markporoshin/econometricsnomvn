package com.p0mami.models.realizations;

import org.apache.commons.math3.util.Pair;

import java.util.stream.Stream;

public abstract class StochasticProcessStreamRealization {

    public abstract Stream<Pair<Double, Double>> getPointsStream();

    public abstract long getSize();

    public abstract void setSize(long size);

    public abstract void close();

}

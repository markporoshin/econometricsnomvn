package com.p0mami;

import com.p0mami.models.processes.StochasticProcessGeneral;
import com.p0mami.models.realizations.StochasticProcessStreamFunctionalRealization;
import com.p0mami.services.providers.StochasticProcessRealizationProvider;
import com.p0mami.services.writers.CSVStochasticProcessStreamRealizationWriter;

import java.util.Random;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Supplier;
import java.util.stream.DoubleStream;

public class WriteSampleToFile {

    private static final Random random = new Random();

    public static void main(String[] args) {
        double[] koefs = {1., 0.07, 0.03, 0.09};
        DoubleUnaryOperator g = (t) -> koefs[0] * t*t*t + koefs[1] * t*t + koefs[2] * t + koefs[3];

        double mean = 0;
        double sigma = 1;

        DoubleUnaryOperator e = (t) -> random.nextGaussian() * sigma + mean;

        long size = 10_000_000;
        float step = 0.001f;

        Supplier<DoubleStream> d = () -> {
            random.setSeed(32);
            return DoubleStream.iterate(0, c -> c + step).sequential().limit(size);
        };
        StochasticProcessGeneral process = new StochasticProcessGeneral(g, e);
        StochasticProcessRealizationProvider provider = new StochasticProcessRealizationProvider(process);
        StochasticProcessStreamFunctionalRealization generalRealization = provider.provide(d, (int)(size * step));
        CSVStochasticProcessStreamRealizationWriter writer = new CSVStochasticProcessStreamRealizationWriter();
        writer.write(generalRealization, size, "results/exp4/input.csv");
    }

}

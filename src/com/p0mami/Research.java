package com.p0mami;

import com.p0mami.models.processes.StochasticProcessGeneral;
import com.p0mami.models.processes.StochasticProcessPolyApproximation;
import com.p0mami.models.realizations.StochasticProcessGeneralRealization;
import com.p0mami.models.realizations.StochasticProcessStreamFunctionalRealization;
import com.p0mami.services.NormalDistributionWithEjection;
import com.p0mami.services.approximation.PolyApproximationService;
import com.p0mami.services.providers.StochasticProcessRealizationProvider;
import com.p0mami.services.tests.MannWhitneyTest;
import com.p0mami.services.tests.SmirnovTest;
import org.apache.commons.math3.util.Pair;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Research {
    private static BufferedWriter researchWriter;

    static {
        try {
            researchWriter = new BufferedWriter(new FileWriter("research10.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static final Random random = new Random();
    public static void main(String[] args) throws IOException {
        double[] koefs = {1., 0.07, 0.03, 0.09};
        double mean = 0;
        double sigma = 10;
        NormalDistributionWithEjection distribution = new NormalDistributionWithEjection(0, sigma);
        DoubleUnaryOperator g = (t) -> koefs[0] * t*t*t + koefs[1] * t*t + koefs[2] * t + koefs[3];
        DoubleUnaryOperator e = (t) -> distribution.nextGaussian();
        double step = 0.001;
        int n = 200;
        for (int i = 0; i < 5000; i++) {
            double a = n * step * i;
            double b = n * step * (i + 1);
            researchWriter.write(
                    String.format("{\n\ta=%f; b=%f", a, b)
            );
            List<Double> d = DoubleStream.iterate(a, c -> c + step).limit(n).boxed().collect(Collectors.toList());
            StochasticProcessGeneral process = new StochasticProcessGeneral(g, e);
            StochasticProcessRealizationProvider provider = new StochasticProcessRealizationProvider(process);
            StochasticProcessGeneralRealization generalRealization = provider.provide(d);
            PolyApproximationService approximationService = new PolyApproximationService();
            for (int k = 0; k <= 3; k++) {

                StochasticProcessPolyApproximation approximation = approximationService.approximate(generalRealization, k);
                double[] diffs = generalRealization.getPoints().stream()
                        .mapToDouble((p)-> p.getSecond() - approximation.getX(p.getFirst())).toArray();
                int diffSize = diffs.length;
                double[] firstPart = Arrays.copyOfRange(diffs, 0, diffSize / 2);
                double[] secondPart = Arrays.copyOfRange(diffs, diffSize / 2, diffSize);

                MannWhitneyTest mannWhitneyTest = new MannWhitneyTest();
                SmirnovTest smirnovTest = new SmirnovTest();
                mannWhitneyTest.setLogging(false);
                smirnovTest.setLogging(false);
                Pair<Boolean, Double> mannWhithneyResult = mannWhitneyTest.test(firstPart, secondPart, 0.05);
                Pair<Boolean, Double> smirnovResult = smirnovTest.test(firstPart, secondPart, 0.05);
                researchWriter.write(
                            String.format("\n\tk=%d; mw_test={%b, %f}; sm_test={%b, %f}; koef=%s; sigma=%f",
                                    k,
                                    mannWhithneyResult.getFirst(), mannWhithneyResult.getSecond(),
                                    smirnovResult.getFirst(), smirnovResult.getSecond(),
                                    approximation.getBettaStr(),
                                    approximation.getSigma()
                            )
                );
            }
            researchWriter.write("\n}\n");
        }
        researchWriter.close();
    }

    private static void draw(StochasticProcessGeneralRealization generalRealization, StochasticProcessPolyApproximation approximation) {
        StochasticProcessRealizationProvider approximationProvider = new StochasticProcessRealizationProvider(approximation);
        StochasticProcessGeneralRealization approximationRealization = approximationProvider.provide(
                Arrays.stream(generalRealization.getT()).boxed().collect(Collectors.toList())
        );
        XYSeries generalSeries = new XYSeries("general");
        generalRealization.getPoints().forEach(point -> generalSeries.add(point.getFirst(), point.getSecond()));
        XYSeries approximationSeries = new XYSeries("approximation");
        approximationRealization.getPoints().forEach(point -> approximationSeries.add(point.getFirst(), point.getSecond()));

        XYSeriesCollection collection = new XYSeriesCollection(generalSeries);
        collection.addSeries(approximationSeries);

        JFreeChart generalChart = ChartFactory
                .createXYLineChart("Stochastic Process Realization", "t", "X",
                        collection,
                        PlotOrientation.VERTICAL,
                        true, true, true);

        JFrame frame = new JFrame("Window");
        frame.setLayout( new FlowLayout() );
        frame.getContentPane().add(new ChartPanel(generalChart));
        frame.pack();
        frame.setSize(400,300);
        frame.show();
    }

}
